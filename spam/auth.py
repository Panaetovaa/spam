import logging

from starlette.requests import Request

from spam.models import Users
from spam.models import Sessions


logger = logging.getLogger(__name__)


class AuthError(Exception):
    pass


class NotAuthenticatedError(AuthError):
    pass


class Authenticator:
    async def __call__(self, request: Request) -> Users:
        session_id = request.cookies.get('spam-session')
        if session_id:
            session = await Sessions.get_active(session_id)
            if not session:
                raise NotAuthenticatedError()
        else:
            raise NotAuthenticatedError()

        user = await Users.get(session.user_id)
        return user
