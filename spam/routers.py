import logging
import pkg_resources

from typing import Dict
from typing import List

from fastapi import APIRouter
from fastapi.responses import HTMLResponse
from fastapi import Response
from fastapi import Depends
from pydantic import BaseModel

from spam.exceptions import APIError
from spam.auth import Authenticator
from spam.models import Invoices
from spam.models import Sessions
from spam.models import Users


logger = logging.getLogger(__name__)
router = APIRouter()


@router.get("/", include_in_schema=False)
def view_root(user=Depends(Authenticator())):
    return HTMLResponse(pkg_resources.resource_string(__name__, 'frontend/index.html'))


@router.get("/login", include_in_schema=False)
def view_login():
    return HTMLResponse(pkg_resources.resource_string(__name__, 'frontend/login.html'))


class LoginRequest(BaseModel):
    login: str = ''
    password: str = ''


@router.post("/login")
async def view_authenticate(body: LoginRequest, response: Response):
    user = await Users.authenticate(body.login, body.password)

    if not user:
        raise APIError(401, 'failed-authentication')

    session = await Sessions.create_new(user.id)
    response.set_cookie(key="spam-session", value=session.session_id)
    return {'success': True}


class InvoiceModel(BaseModel):
    id: int
    issuer: str
    amount: float
    currency: str
    account: str
    status: str
    approval_id: str
    approver_check_data: Dict


class FilterResponse(BaseModel):
    draw: str = '0'
    data: List[InvoiceModel]
    recordsTotal: int
    recordsFiltered: int


@router.get("/invoices", response_model=FilterResponse)
async def view_filter(
        user=Depends(Authenticator()),
        draw: str = '0', offset: str = '0', limit: str = '50',
):
    offset, limit = _parse_filter_parameters(offset, limit)
    invoices = await Invoices.query.order_by(Invoices.id.desc())\
        .limit(limit)\
        .offset(offset).gino.all()

    count = await Invoices.count()

    data = []
    for invoice in invoices:
        data.append(InvoiceModel(
            id=invoice.id,
            issuer='Alexey Panaetov',
            amount=invoice.amount,
            currency=invoice.currency,
            account=invoice.currency,
            status=invoice.status,
            approval_id=(await invoice.format_approval()),
            approver_check_data=(await invoice.get_approver_check_data()) or {},
        ).dict())

    return {
        'draw': draw,
        'data': data,
        'recordsTotal': count,
        'recordsFiltered': count,
    }


class EvaluateInvoiceResponse(BaseModel):
    id: int


@router.post("/invoices/{invoice_id}/evaluate", response_model=EvaluateInvoiceResponse)
async def view_evaluate(
        invoice_id,
        user=Depends(Authenticator()),
):
    invoice_id = int(invoice_id)
    invoice = await Invoices.get(invoice_id)
    await invoice.evaluate_status()
    return {
        'id': invoice.id,
    }


class CreateInvoiceRequest(BaseModel):
    amount: float
    currency: str
    account: str


class CreateInvoiceResponse(BaseModel):
    id: int


@router.post("/invoices", response_model=CreateInvoiceResponse)
async def view_create(
        body: CreateInvoiceRequest,
        user=Depends(Authenticator()),
):
    invoice = await Invoices.create(issuer='alexey.panaetov', **body.dict())
    return {
        'id': invoice.id,
    }


def _parse_filter_parameters(offset, limit):
    try:
        offset = int(offset)
    except ValueError:
        offset = 0

    try:
        limit = int(limit)
    except ValueError:
        limit = 10

    return offset, limit
