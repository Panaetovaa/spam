$(function() {
    var login_input = $('form input[name="login"]');
    var password_input = $('form input[name="password"]');
    var error_div = $('form .error-message');

    $("#login-button").button().on("click", function(e) {
        e.preventDefault();
        submit_form();
    });

    var submit_form = function() {
        error_div.hide();

        var payload = {
            'login': login_input.val(),
            'password': password_input.val()
        }
        $.post('/login', JSON.stringify(payload), function(resp) {
            if (resp.success === true) {
                window.location = '/';
            } else if (resp.success === false) {
                error = resp.message || 'Authentication error';
                error_div.html(error);
                error_div.show();
            } else {
                error_div.html('Something went wrong');
                error_div.show();
            }
        }, 'json').fail(function(resp) {
            if (resp.status == 401) {
                message = 'Authentication failed';
            } else {
                message = 'Something went wrong';
            }
            error_div.html(message);
            error_div.show();
        });
    }
});
