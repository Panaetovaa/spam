$(function() {

    $("input[type=submit], button").button();

    $(document).ajaxError(function (event, xhr, ajaxOptions, thrownError) {
        if (xhr.status == 401) {
            // Storage dns are not configured
            window.location = '/login';
        } else {
            PROCESS_UNKNOWN_SERVER_ERROR('Unknown error');
        }
    });

    var Constants = {}
    Constants.FIELD_IS_REQUIRED_ERROR = 'Field is required';

    var DATATABLES_DATA_FUNC = function(d) {
        return {
            draw: d.draw,
            offset: d.start,
            limit: d.length
        }
    }

    var _pad_zeros = function(value, length) {
        var s = value + "";
        while (s.length < length) s = "0" + s;
        return s;
    }

    var PROCESS_UNKNOWN_SERVER_ERROR = function(error_text) {
        var error_div = $("#general-error-wrapper");
        error_div.text(error_text);

        var dialog = error_div.dialog({
            autoOpen: true,
            width: 1000,
            modal: true,
            buttons: {
                OK: function() {
                    dialog.dialog('close');
                }
            }
        })
    }


    var BUILD_FORM = function(options) {
        var scope = {};

        scope.table = options.table;
        var ok_handler = function() {
            var has_errors = validate_form();
            if (has_errors) {
                return;
            }

            options.ok_handler(scope);
        }

        var cancel_handler = function() {
            if (options.cancel_handler) {
                options.cancel_handler(scope);
            }
            scope.dialog.dialog("close");
        }

        scope.process_server_error = function(jqxhr) {
            if (jqxhr.responseJSON && jqxhr.responseJSON.error) {
                var response_json = jqxhr.responseJSON.error;
                if (response_json.type == 'invalid_password') {
                    scope.errors.password.html(response_json.message); 
                    scope.errors.password.show();

                } else if (response_json.type == 'validation_failed') {
                    $.each(response_json.invalid, function(index, el) {
                        scope.errors[el.entry].html(el.message);
                        scope.errors[el.entry].show();
                    });

                } else {
                    PROCESS_UNKNOWN_SERVER_ERROR(response_json.message);
                }
            } else {
                PROCESS_UNKNOWN_SERVER_ERROR(jqxhr.responseText);
            }
        }

        var validate_form = function() {
            var has_errors = false;

            $.each(options.fields, function(name, params) {
                var field = scope.fields[name];
                var get_value = params.get_value || function(f) {
                    return f.val();
                }
                var has_error = false;

                if (params.required && !get_value(field)) {
                    scope.errors[name].html(Constants.FIELD_IS_REQUIRED_ERROR);
                    has_errors = true;
                    has_error = true;
                }

                if (params.validate) {
                    var is_valid;
                    var error_message;
                    [is_valid, error_message] = params.validate(scope);
                    if (!is_valid) {
                        scope.errors[name].show();
                        scope.errors[name].html(error_message);
                        has_errors = true;
                        has_error = true;
                    }
                }

                if (has_error) {
                    scope.errors[name].show();
                } else {
                    scope.errors[name].hide();
                }
            });

            return has_errors;
        }

        scope.dialog = $(options.dialog_selector).dialog({
            autoOpen: false,
            width: 700,
            modal: true,
            buttons: {
                Ok: ok_handler,
                Cancel: cancel_handler,
            },
            close: function() {
                scope.form[0].reset();
                $.each(scope.errors, function(name, elem) {
                    elem.hide();
                });
            },
            open: function() {
                $.each(scope.fields, function(name, elem) {
                    _clear_field(elem);
                });
            }
        });

        var _clear_field = function(f) {
            $(f).val(null).trigger('change');
        }

        scope.form = scope.dialog.find("form").on("submit", function( event ) {
            event.preventDefault();
            ok_handler();
        });

        scope.fields = {};
        scope.errors = {}

        scope.general_error = $(options.dialog_selector + ' .general-error-message');

        $.each(options.fields, function(name, params) {
            var input_type = params.input_type || 'input';
            var field_selector = (
                options.dialog_selector + " " +
                input_type + "[name='" + name + "']"
            );
            scope.fields[name] = $(field_selector);

            if (params.is_date) {
                scope.fields[name].datepicker({dateFormat: 'dd-mm-yy'});
            }

            var error_selector = (
                options.dialog_selector + " .error-message[for='" + name + "']"
            );
            scope.errors[name] = $(error_selector);
        });

        if (options.launch_button_selector) {
            $(options.launch_button_selector).button().on("click", function() {
                scope.dialog.dialog("open");
            });
        }

        return scope;
    }

    $("#create-button").button().on("click", function() {
        create_form.dialog.dialog("open");
    });

    $("#refresh-button").button().on("click", function() {
        table.ajax.reload();
    });

    var salt = 0;

    var table = $('#reports-list').DataTable( {
        processing: true,
        serverSide: true,
        paging: true,
        searching: false,
        ordering:  false,
        info: false,
        ajax: {
            url: "/invoices",
            data: DATATABLES_DATA_FUNC,
            error: function (xhr, error, code) {
                console.log(xhr);
                console.log(code);
            }
        },
        deferRender: true,
        columns: [
            {"data": "id"},  // 0
            {"data": "issuer"},  // 1
            {"data": "amount"},  // 2
            {"data": "currency"},  // 3
            {"data": "account"},  // 4
            {"data": "status"},  // 5
            {"data": "approval_id"},  // 6
            {"data": "approver_check_data"},  // 7
            {"data": "id"},  // 8
        ],
        columnDefs: [
            {
                render: function (data, type, full, meta) {
                    var html = '<pre>' + full.approval_id + '</pre>';
                    return html;
                },
                targets: 6
            },
            {
                render: function (data, type, full, meta) {
                    if (JSON.stringify(data) == '{}') {
                        return "";
                    }
                    var html = '<table class="approver-data">';
                    html += "<tr><td>Can be approved</td><td>" + data.can_be_approved + "</td></tr>";
                    html += "<tr><td>Approved amount</td><td>" + data.approved_amount + "</td></tr>";
                    html += "<tr><td>Rule ID</td><td>" + data.operation_rule_id || '' + "</td></tr>";
                    html += "<tr><td>Scheme</td><td>" + JSON.stringify(data.current_scheme) + "</td></tr>";

                    html += "<tr><td>Daily Rule ID</td><td>" + data.daily_rule_id || '' + "</td></tr>";
                    html += "<tr><td>Daily scheme</td><td>" + JSON.stringify(data.current_day_scheme) + "</td></tr>";
                    html += "</table>"
                    return html;
                },
                targets: 7
            },
            {
                render: function (data, type, full, meta) {
                    salt++;
                    var button_id = "evaluate-" + data + "-button-" + salt;
                    var html = "<button id='" + button_id + "'>";
                    html += "Evaluate";
                    html += "</button>";

                    table.on('draw', function() {
                        $("#" + button_id).button().on("click", function() {
                            $.ajax({
                                url: "/invoices/" + full.id + "/evaluate",
                                type: 'POST',
                                success: function(resp) {
                                    table.ajax.reload();
                                },
                                error: function(jqXHR) {
                                    scope.process_server_error(jqXHR);
                                }
                            });
                        });
                    });

                    return html;
                },
                targets: 8
            },
        ]
    });

    var create_form = BUILD_FORM({
        dialog_selector: "#create-form-wrapper",
        table: table,
        fields: {
            amount: {
                required: true
            },

            currency: {
                required: true
            },

            account: {
                required: true
            }
        },

        ok_handler: function(scope) {
            var payload = {
                'amount': parseInt(scope.fields.amount.val()),
                'currency': scope.fields.currency.val(),
                'account': scope.fields.account.val()
            }

            $.ajax({
				url: "/invoices/",
				type: 'POST',
				data: JSON.stringify(payload),
				success: function(resp) {
                	table.ajax.reload();
                	scope.dialog.dialog("close");
            	},
				error: function(jqXHR) {
                	scope.process_server_error(jqXHR);
            	}
			});
        }
    });

    var resolve_form = BUILD_FORM({
        dialog_selector: "#resolve-form-wrapper",
        table: table,
        fields: {
            comment: {
                required: true,
                input_type: 'textarea'
            },
            report_id: {
                required: true
            }
        },

        ok_handler: function(scope) {
            var payload = {
                'comment': scope.fields.comment.val(),
            }

            $.ajax({
				url: "/reports/resolve/" + scope.fields.report_id.val(),
				type: 'PUT',
				data: JSON.stringify(payload),
				success: function(resp) {
                	scope.table.ajax.reload();
                	scope.dialog.dialog("close");
            	},
				error: function(jqXHR) {
                	scope.process_server_error(jqXHR);
            	}
			});
        }
    });

    var block_form = BUILD_FORM({
        dialog_selector: "#block-form-wrapper",
        table: table,
        fields: {
            comment: {
                required: true,
                input_type: 'textarea'
            },
            report_id: {
                required: true
            }
        },

        ok_handler: function(scope) {
            var payload = {
                'comment': scope.fields.comment.val(),
            }
            $.ajax({
				url: "/reports/block/" + scope.fields.report_id.val(),
				type: 'PUT',
				data: JSON.stringify(payload),
				success: function(resp) {
                	scope.table.ajax.reload();
                	scope.dialog.dialog("close");
            	},
				error: function(jqXHR) {
                	scope.process_server_error(jqXHR);
            	}
			});
        }
    });

});
