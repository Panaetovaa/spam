import os

DB_DSN = os.getenv("SPAM_DB_DSN", "postgresql://postgres:postgres@db:5432/spam")

DEBUG = os.getenv("SPAM_DEBUG", "1").lower() in ('1', 'true', 't')
DOCS_ENABLE = os.getenv("SPAM_DOCS_ENABLE", "1").lower() in ('1', 'true', 't')

STORAGE_URL = os.getenv("SPAM_STORAGE_URL", "http://admin:admin@localhost:8001")
APPROVER_URL = os.getenv("SPAM_APPROVER_URL", "http://localhost:8020")

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'console': {
            'level': os.getenv('SPAM_LOGGERS_HANDLERS_LEVEL', 'INFO'),
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': os.getenv('SPAM_LOGGERS_HANDLERS_LEVEL', 'INFO'),
        },
        'gino': {
            'level': os.getenv('SPAM_LOGGERS_GINO_LEVEL', 'INFO'),
        }
    }
}

if os.getenv("SPAM_GRAYLOG_HOST", ""):
    LOGGING['handlers']['graylog'] = {
        "level": os.getenv("SPAM_LOG_LEVEL_GRAYLOG", "INFO"),
        "class": "pygelf.handlers.GelfUdpHandler",
        "formatter": "default",
        "host": os.getenv("SPAM_GRAYLOG_HOST", "graylog"),
        "port": int(os.getenv("SPAM_GRAYLOG_PORT", "12201")),
        "compress": bool(os.getenv("SPAM_GRAYLOG_COMPRESS", "True")),
        "_environment": os.getenv("SPAM_GRAYLOG_ENVIRONMENT", "SECENV"),
        "_app_name": "spam",
        "include_extra_fields": True,
        "_container_id": os.getenv("HOSTNAME"),
        "_service": "none",
    }
    LOGGING['loggers']['']['handlers'].append('graylog')


SENTRY_DSN = os.getenv('SPAM_LOGGING_SENTRY_DSN')
