"""init

Revision ID: 7040fdfd424e
Revises: 
Create Date: 2020-07-20 18:46:47.695790

"""

# revision identifiers, used by Alembic.
revision = '7040fdfd424e'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('reports',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('created_at', sa.Float(), nullable=True),
    sa.Column('updated_at', sa.Float(), nullable=True),
    sa.Column('state', sa.Enum('open', 'blocked', 'resolved', name='states'), nullable=False),
    sa.Column('resource_id', sa.String(length=36), nullable=False),
    sa.Column('report_id', sa.String(length=36), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('created_at', sa.Float(), nullable=True),
    sa.Column('updated_at', sa.Float(), nullable=True),
    sa.Column('login', sa.String(length=128), nullable=False),
    sa.Column('password', sa.String(length=128), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('login')
    )
    op.create_table('reports_history',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('created_at', sa.Float(), nullable=True),
    sa.Column('report_id', sa.Integer(), nullable=True),
    sa.Column('state', sa.Enum('open', 'blocked', 'resolved', name='states'), nullable=False),
    sa.Column('staff_id', sa.Integer(), nullable=False),
    sa.Column('staff_comment', sa.Text(), server_default='', nullable=False),
    sa.ForeignKeyConstraint(['report_id'], ['reports.id'], ),
    sa.ForeignKeyConstraint(['staff_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('reports_history')
    op.drop_table('users')
    op.drop_table('reports')
    # ### end Alembic commands ###
