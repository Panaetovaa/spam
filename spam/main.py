import logging
import logging.config

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from spam import db
from spam import exceptions
from spam import routers
from spam import sentry
from spam import settings
from spam.version import __version__


def init_app():
    logging.config.dictConfig(settings.LOGGING)
    sentry.setup()

    app = FastAPI(
        title='STORAGE',
        debug=settings.DEBUG,
        version=__version__,
        docs_url="/docs" if settings.DOCS_ENABLE else None,
        redoc_url="/redoc" if settings.DOCS_ENABLE else None,
    )

    exceptions.setup_handlers(app)

    app.mount("/static", StaticFiles(directory="spam/frontend"), name="frontend")

    app.on_event("shutdown")(_on_shutdown)
    app.on_event("startup")(_on_startup)

    app.include_router(routers.router)
    return app


async def _on_startup():
    await db.startup(settings.DB_DSN)


async def _on_shutdown():
    await db.shutdown()


app = init_app()
