import logging
import re

from gino import Gino


logger = logging.getLogger(__name__)


db = Gino()


async def startup(dsn):
    dsn = str(dsn)
    dsn_without_credentials = _get_dsn_without_credentials(dsn)
    if dsn_without_credentials:
        logger.info(f"Connection to db: {dsn_without_credentials}")
    else:
        logger.info("Cannot log database DSN because credentials will be printed.")

    await db.set_bind(dsn, logging_name='gino')


async def shutdown():
    bind = db.pop_bind()

    if bind:
        await bind.close()


def _get_dsn_without_credentials(dsn):
    matched = re.match('[^@]+@(.*(:.*)?\/.*)', dsn)
    if matched:
        return matched.group(1)
    return ""
