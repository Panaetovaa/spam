import logging
from typing import Any, Dict, List, Optional

from fastapi.exceptions import RequestValidationError

from pydantic import ValidationError as PydanticValidationError

from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse
from starlette.responses import RedirectResponse
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR

from spam.auth import NotAuthenticatedError


logger = logging.getLogger(__name__)


def setup_handlers(app):

    app.middleware('http')(middleware_process_error)
    app.exception_handler(NotAuthenticatedError)(exception_handler)


class APIError(Exception):
    error_code = 'unknown-error'

    def __init__(
            self,
            status_code: int = None,
            error_code: str = '',
            message: Optional[str] = '',
            details: Optional[List[Dict[str, Any]]] = None,
    ):
        self.status_code = status_code or self.__class__.status_code
        self.error_code = error_code or self.__class__.error_code
        self.message = message
        self.details = details or []
        super().__init__()

    def as_dict(self) -> Dict[str, Any]:
        if not self.message and self.details:
            details = [row.get("message").rstrip('.') for row in self.details or [] if row.get("message")]
            message = ". ".join(details)
        else:
            message = str(self.message or 'Panic! Something went wrong.')

        return {"code": self.error_code, "message": message, "details": self.details}


class ValidationFailed(APIError):
    status_code = HTTP_422_UNPROCESSABLE_ENTITY
    error_code = 'validation-error'

    def set_details(self, details: List[Dict[str, Any]]):
        self.details = normalize_details(details)


async def middleware_process_error(request, call_next):
    try:
        return await call_next(request)
    except Exception as exc:
        return await exception_handler(request, exc)


async def exception_handler(request, exc):
    if isinstance(exc, NotAuthenticatedError):
        if request.url.path.startswith('/reports'):
            error = APIError(status_code=401)
            return JSONResponse(error.as_dict(), status_code=error.status_code)
        else:
            return RedirectResponse(url='/login')

    elif isinstance(exc, APIError):
        return JSONResponse(exc.as_dict(), status_code=exc.status_code)

    elif isinstance(exc, HTTPException):
        error = APIError(status_code=exc.status_code)
        return JSONResponse(error.as_dict(), status_code=error.status_code)

    elif isinstance(exc, (RequestValidationError, PydanticValidationError)):
        error = ValidationFailed()
        error.set_details(exc.errors())
        return JSONResponse(error.as_dict(), status_code=error.status_code)

    else:
        logger.exception(f'Request failed because of error: {exc}')
        error = APIError(status_code=HTTP_500_INTERNAL_SERVER_ERROR)
        return JSONResponse(error.as_dict(), status_code=error.status_code)


def normalize_details(details: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    return [
        {
            "code": data["type"].upper(),
            "field": ".".join([loc for loc in data["loc"] if isinstance(loc, str)]),
            "message": data["msg"],
        }
        for data in details
    ]
