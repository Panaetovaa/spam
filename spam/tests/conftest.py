import asyncio
import os

import psycopg2

from contextlib import suppress

import pytest

from spam.models import Users
from spam import main


@pytest.fixture(autouse=True)
async def setup(db_conn, user_login, user_hash_password):
    cur = db_conn.cursor()

    cur.execute("delete from sessions;")
    cur.execute("delete from reports_history;")
    cur.execute("delete from reports;")
    cur.execute("delete from users;")

    cur.execute("""
        insert into users ("login", "password") values
        (%s, %s);
    """, (user_login, user_hash_password))

    db_conn.commit()


@pytest.fixture(autouse=True)
async def teardown():
    yield

    pending = asyncio.Task.all_tasks()
    for task in pending:
        task.cancel()
        with suppress(asyncio.CancelledError):
            await task


@pytest.fixture
def user_login():
    return "admin"


@pytest.fixture
def user_password():
    return "admin"


@pytest.fixture
def user_hash_password(user_password):
    return Users.hash_password(user_password)


@pytest.fixture
def db_conn():
    db_dsn = os.environ.get('SPAM_DB_DSN', 'postgresql://postgres:postgres@db/spam')
    conn = psycopg2.connect(db_dsn)
    yield conn
    conn.close()


@pytest.fixture
def user_id(db_conn):
    cur = db_conn.cursor()

    cur.execute("select id from users limit 1;")
    yield cur.fetchall()[0][0]
    cur.close()
