import pytest
from fastapi.testclient import TestClient
from spam.main import app


@pytest.mark.authentication
def test_success_authentication(user_login, user_password):
    with TestClient(app) as client:
        response = client.post("/login", json={
            'login': user_login,
            'password': user_password,
        })
        assert response.status_code == 200


@pytest.mark.authentication
def test_failure_authentication(user_login, user_password):
    with TestClient(app) as client:
        response = client.post("/login", json={
            'login': user_login,
            'password': 'test123',
        })
        assert response.status_code == 401


@pytest.mark.reports
def test_filter_reports(db_conn, user_login, user_password):
    cur = db_conn.cursor()

    cur.execute(
        "insert into reports (state, resource_id, report_id) values "
        "('open', 'r1', '1'), ('open', 'r2', '2'), ('open', 'r3', '3');"
    )
    db_conn.commit()
    cur.close()

    with TestClient(app) as client:
        client.post("/login", json={
            'login': user_login,
            'password': user_password,
        })
        response = client.get("/reports/filter")
        assert response.status_code == 200
        assert len(response.json()['data']) == 3

        response = client.get("/reports/filter?limit=2")
        assert response.status_code == 200
        assert len(response.json()['data']) == 2


@pytest.mark.reports
def test_block_report(db_conn, user_login, user_password):
    cur = db_conn.cursor()

    cur.execute(
        "insert into reports (state, resource_id, report_id) values "
        "('open', 'r1', '1') returning id, report_id;"
    )
    id, report_id = cur.fetchone()
    db_conn.commit()

    with TestClient(app) as client:
        client.post("/login", json={
            'login': user_login,
            'password': user_password,
        })
        response = client.put(f"/reports/block/{report_id}", json={'comment': 'test'})
        assert response.status_code == 200

    cur.execute("select state from reports where report_id = %s", [report_id])
    assert cur.fetchone()[0] == 'blocked'

    with TestClient(app) as client:
        client.post("/login", json={
            'login': user_login,
            'password': user_password,
        })
        response = client.put(f"/reports/resolve/{report_id}", json={'comment': 'test'})
        assert response.status_code == 200

    cur.execute("select state from reports where report_id = %s", [report_id])
    assert cur.fetchone()[0] == 'resolved'

    cur.execute("select count(*) from reports_history where report_id = %s", [id])
    assert cur.fetchone()[0] == 2

    cur.close()
