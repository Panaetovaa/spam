import sentry_sdk

from sentry_sdk.integrations.logging import LoggingIntegration

from spam import settings


def setup():
    if not settings.SENTRY_DSN:
        return

    sentry_sdk.init(
        dsn=str(settings.SENTRY_DSN),
        integrations=[LoggingIntegration()]
    )
