import json
import logging
import requests
import time
import uuid

import hashlib

import sqlalchemy as sa
from sqlalchemy.schema import UniqueConstraint

from spam.db import db
from spam import settings


logger = logging.getLogger(__name__)


class Sessions(db.Model):
    __tablename__ = 'sessions'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    created_at = db.Column(db.Float, default=time.time)
    updated_at = db.Column(db.Float, default=time.time, onupdate=time.time)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    expire_at = db.Column(db.Float, nullable=False)
    session_id = db.Column(db.String(32), nullable=False)

    @classmethod
    async def create_new(cls, user_id, duration=3600 * 24):
        return await cls.create(
            user_id=user_id, expire_at=time.time() + duration,
            session_id=uuid.uuid4().hex,
        )

    @classmethod
    async def get_active(cls, session_id):
        return await cls.query\
            .where(cls.session_id == session_id)\
            .where(cls.expire_at > time.time())\
            .gino.first()


class Users(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    created_at = db.Column(db.Float, default=time.time)
    updated_at = db.Column(db.Float, default=time.time, onupdate=time.time)

    login = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(128), nullable=False)

    __table_args__ = (UniqueConstraint('login'))

    @classmethod
    def hash_password(cls, password: str) -> str:
        return hashlib.sha256(password.encode()).hexdigest()

    @classmethod
    async def authenticate(cls, login: str, password: str):
        return await cls.query.where(cls.login == login)\
            .where(cls.password == cls.hash_password(password))\
            .gino.first()


class StatusEnum:
    draft = 'draft'
    waiting_assign = 'waiting_assign'
    waiting_approve = 'waiting_approve'
    ready_for_processing = 'ready_for_processing'
    done = 'done'


class ApproverChecks(db.Model):
    __tablename__ = 'approver_checks'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    created_at = db.Column(db.Float, default=time.time)
    updated_at = db.Column(db.Float, default=time.time, onupdate=time.time)

    invoice_id = db.Column(db.Integer, db.ForeignKey('invoice.id'), nullable=False)
    data = db.Column(db.JSON(), nullable=False)


class Invoices(db.Model):
    __tablename__ = 'invoice'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    created_at = db.Column(db.Float, default=time.time)
    updated_at = db.Column(db.Float, default=time.time, onupdate=time.time)

    issuer = db.Column(db.String(128), nullable=False, server_default='alexey.panaetov')

    amount = db.Column(db.Float, nullable=False)
    currency = db.Column(db.String(3), nullable=False)
    account = db.Column(db.String(16), nullable=False)
    status = db.Column(db.String(128), server_default=StatusEnum.draft)

    approval_id = db.Column(db.Integer, nullable=True)

    async def get_approver_check_data(self):
        check = await ApproverChecks.query\
            .where(ApproverChecks.invoice_id == self.id)\
            .order_by(ApproverChecks.id.desc())\
            .gino.first()

        if check:
            return check.data

    async def evaluate_status(self):
        if self.status == StatusEnum.draft:
            await self.evaluate_draft()

        elif self.status == StatusEnum.waiting_assign:
            await self.evaluate_waiting_assign()

        elif self.status == StatusEnum.waiting_approve:
            await self.evaluate_waiting_approve()

        elif self.status == StatusEnum.ready_for_processing:
            await self.evaluate_ready_for_processing()

    async def get_approval(self):
        resp = requests.post(settings.STORAGE_URL + '/approval/filter', json={
            'filters': {
                'id': [self.approval_id],
            }
        })
        return resp.json()['entities'][0]

    async def get_issuer_certificate(self):
        approval = await self.get_approval()

        resp = requests.post(settings.STORAGE_URL + '/certificate/filter', json={
            'filters': {
                'public_key_hash': [approval['issuer_public_key_hash']],
            }
        })
        return resp.json()['entities'][0]

    async def format_approval(self):
        parts = []
        await self._format_approval_parts(parts)
        return "\n".join(parts)

    async def _format_approval_parts(self, parts):
        if not self.approval_id:
            return []

        if self.approval_id:
            parts.append(f'APPROVAL-{self.approval_id}')
            if self.status == StatusEnum.waiting_assign:
                return

            cert = await self.get_issuer_certificate()
            parts.append(
                f"Issuer certificate: ident={cert['ident']}, "
                f"role={cert['role']}, public_key_hash={cert['public_key_hash']}."
            )

    async def evaluate_waiting_assign(self):
        approval = await self.get_approval()
        if approval['issuer_signature']:
            await self.update(status=StatusEnum.waiting_approve).apply()

    async def evaluate_draft(self):
        if self.approval_id:
            await self.update(status=StatusEnum.waiting_assign).apply()

        else:
            signable = {
                'amount': self.amount,
                'currency': self.currency,
                'invoice_id': self.id,
                'extra': {
                    'account': self.account,
                },
            }
            resp = requests.post(settings.STORAGE_URL + '/approval/create', json={
                'entity': {
                    'operation': 'manual-invoice',
                    'details': json.dumps(signable)
                }
            })
            if resp.status_code != 200:
                logger.info(f"Cannot create approval: storage reply= {resp.json()}")
                resp.raise_for_status()

            await self.update(
                approval_id=resp.json()['id'],
                status=StatusEnum.waiting_assign,
            ).apply()

    async def evaluate_waiting_approve(self):
        resp = requests.post(settings.APPROVER_URL + '/check', json={
            'approval_id': self.approval_id,
        })
        resp.raise_for_status()
        data = resp.json()['result']
        await ApproverChecks.create(data=data, invoice_id=self.id)

        if data['can_be_approved']:
            resp = requests.post(settings.APPROVER_URL + '/request', json={
                'approval_id': self.approval_id,
            })
            resp.raise_for_status()
            await self.update(status=StatusEnum.ready_for_processing).apply()

    async def evaluate_ready_for_processing(self):
        await self.update(status=StatusEnum.done).apply()

    @classmethod
    async def count(cls, query=None):
        query = query or cls.query
        query = query.with_only_columns([sa.func.count()])

        # FIXME
        # workaround for error
        # AttributeError: 'NoneType' object has no attribute 'scalar'
        query = query.where(cls.id > 0)

        return await query.gino.scalar()
