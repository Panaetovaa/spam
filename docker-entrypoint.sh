#!/usr/bin/env bash

case "$1" in
    "help")
        echo "Please use of next parameters to start: "
        echo "  > webserver: Start webserver"
        echo "  > bash: Start bash shell"
        echo "  > migrate: Migrate database"
        ;;
    "bash")
        echo "Starting bash ..."
        exec bash
        ;;
    "migrate")
        echo "Database migrate"
        alembic upgrade head
        ;;

    "webserver")
        echo "Starting webserver ..."
        exec uvicorn spam.main:app --host 0.0.0.0 --port 8000
        ;;
    *)
        echo "Unknown command '$1'. please use one of: [webserver, migrate, bash, help]"
        exit 1
        ;;
esac
