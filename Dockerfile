FROM python:3.7

ARG NEXUS_PYPI_INDEX_URL

ENV HOME="/dude"
ENV PYTHONPATH=$HOME/spam

RUN groupadd -g 999 dudes && \
    useradd -u 999 -m -g dudes -s /bin/bash dude && \
    mkdir -p ${HOME}

WORKDIR ${HOME}/spam

COPY --chown=dude:dudes . ${HOME}/spam

RUN pip install -i "${NEXUS_PYPI_INDEX_URL:-https://pypi.org/simple}" -r "$HOME/spam/requirements.txt"

USER dude
ENTRYPOINT ["/dude/spam/docker-entrypoint.sh"]
CMD ["help"]
